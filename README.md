# img_gd

construire des images: png, jpg, jpeg et histogramme avec la livrairie gd

## Installation Pre-requis

```
apt-get install -y libfreetype6-dev libjpeg62-turbo libjpeg62-turbo-dev libjpeg-dev libpng-dev
docker-php-ext-configure gd
docker-php-ext-install gd
```
